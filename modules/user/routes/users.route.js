let path = require('path');
let mongoose = require('mongoose');
let config = require(path.resolve('./config/config'));
let userController = require(path.resolve('./modules/user/controllers/users.controler'));

module.exports.init = function (app) {
  // Carriers collection routes
  app.route('/api/users')
    .get(userController.listUser)
    .post(userController.createUser);
};



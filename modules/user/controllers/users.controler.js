'use strict';

/**
 * Module dependencies.
 */
var path = require('path');
var mongoose = require('mongoose');
var config = require(path.resolve('./config/config'));
var UsersMD = require(path.resolve('./modules/user/models/users.model'));

exports.createUser = function (req, res) {
  try{
    let data = req.body;
    let user = new UsersMD(data);
    user.save();
    return res.status(200).json({data: data});
  } catch (e) {
    console.error('e ', e);
    return res.status(400).json({error: true});
  }

};

exports.listUser = function (req, res) {
  let data = req.body;
  return res.status(200).json({data: data});
};